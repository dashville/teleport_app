import React from 'react'
import {StyleSheet, Text, View} from 'react-native';
import _ from 'lodash';
import {AppStyles, AppColors} from '../../AppStyling'

const ErrorLabel = (props) => (
    <View>
        <Text style={styles.errorText}>{'* ' + props.message}</Text>
    </View>
);

const FormField = (props) => (
    <View style={styles.container}>
        {props.children}
        {props.error ? <ErrorLabel message={props.error}/> : null}
    </View>
);

const styles = StyleSheet.create({
    contianer: {
        flexDirection: 'column'
    },
    errorText: _.extend({}, AppStyles.textBold, {color: AppColors.danger,fontSize: 10})
});

export default FormField;