import React from 'react'
import {
    View,
    TextInput,
    Text,
} from 'react-native';
import {AppStyles, AppColors} from '../../AppStyling'
import _ from 'lodash'
export default class FloatLabelTextInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isFocused: false,
            value: props.value || ''
        }
    }

    focus() {
        this._handleFocus()
    }

    _handleFocus() {
        this.setState({isFocused: true})
    }

    _handleBlur() {
        this.setState({isFocused: false})
    }


    render() {
        const {
            label,
            onChangeText,
            keyboardType,
            onSubmitEditing,
            secureTextEntry,
            placeholder,
            ...props
        } = this.props;
        const {textBold} = AppStyles
        const {isFocused, value} = this.state
        const hasText = _.size(value.trim()) > 0;
        const labelStyle = _.extend({}, textBold, {
            position: 'absolute',
            left: 0,
            top: this.props.labelFixed || hasText || isFocused ? 0 : 20,
            color: !isFocused ? '#aaa' : AppColors.info,
        })
        return (
            <View
                style={{paddingTop: 10, borderBottomWidth: 1, borderBottomColor: !isFocused ? '#aaa' : AppColors.infos}}>
                <Text style={labelStyle}>
                    {label}
                </Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    {this.props.leftButton ? this.props.leftButton(): null}
                    <TextInput
                        ref="input"
                        underlineColorAndroid={'transparent'}
                        style={_.extend({}, textBold, {
                            height: 38,flex:1,
                            color: '#000',

                        })}
                        onChangeText={(value) => {
                            this.setState({value});
                            onChangeText && onChangeText(value);
                        }}
                        placeholder={placeholder}
                        placeholderTextColor={'#eeeeee'}
                        value={this.state.value}
                        secureTextEntry={secureTextEntry}
                        keyboardType={keyboardType}
                        onFocus={this._handleFocus.bind(this)}
                        onBlur={this._handleBlur.bind(this)}
                        onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
                    />
                    {this.props.rightButton ? this.props.rightButton(): null}
                </View>
            </View>
        )
    }
}
TextInput.getDefaultProps({
    secureTextEntry: false,
    placeholder: ''
})