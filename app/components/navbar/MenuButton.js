import React  from 'react'
import {TouchableOpacity, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import {AppColors} from '../../AppStyling'

export default class MenuButton extends React.Component {
    constructor(props) {
        super(props)
    }

    _onMenuPress() {
        const {navigation} = this.props
        navigation && navigation.push('menu')
    }

    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={this._onMenuPress.bind(this)}
            >
                <Icon name={"ios-menu"} size={30} style={styles.icon}/>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 2,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        color: AppColors.primary
    }
})