import React, {Component} from 'react'
import {View, Text, NetInfo, StyleSheet} from 'react-native';
import {AppColors, AppStyles} from './AppStyling'
import _ from 'lodash'

export default class ConnectionPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: null,
        }
    }

    componentDidMount() {
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this._filterConnection(connectionInfo)
        })

        NetInfo.addEventListener('connectionChange', this._connectionChange.bind(this))
    }

    componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this._connectionChange.bind(this))
    }

    _connectionChange(change) {
        this._filterConnection(change)
    }

    _filterConnection(change) {
        switch (change.type) {
            case 'cellular':
            case 'wifi':
                this.setState({isConnected: true})
                break;
            default:
                this.setState({isConnected: false})
                break;
        }
    }


    render() {
        const {isConnected} = this.state
        if (isConnected) {
            return null
        }
        return (
            <View style={styles.panel}>
                <Text style={styles['panel-text']}>No connection</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    panel: {
        height: 25,
        backgroundColor: AppColors.danger,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    'panel-text': _.extend({}, AppStyles.textBold, {
        color: AppColors.white
    })
});