import React from 'react'
import {
    Alert,
    AsyncStorage,
    Dimensions,
    Image,
    PixelRatio,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import _ from 'lodash'
import {AppColors, AppStyles} from './AppStyling';
import Camera from './detection/Camera';
import AuthUtils from './utils/AuthUtils';
import AppUtils from './utils/AppUtils';
import {CachedImage}from 'react-native-cached-image';
import MenuButton from './components/navbar/MenuButton'

export default class WelcomeView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isShowingCamera: false,
            image: null, pin: null
        }
    }

    static navigationOptions = (props) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        this._retrievePin();
    }

    _continueHandler() {
        this.setState({isShowingCamera: true})
    }

    _onCloseCamera() {
        this.setState({isShowingCamera: false})
    }

    _onUploadImage(data) {
        const {app} = this.props.screenProps
        const {image} = this.state
        const onSuccess = (responseData) => {
            if (responseData.success) {
                this.setState({image: responseData.item})
            } else {
                Alert.alert("Error", JSON.stringify(responseData.message))
            }
        };
        const onError = (err) => {
            Alert.alert("Error", JSON.stringify(err))
            console.log(' WelcomeView._onUploadImage.error ===>', err)
        }
        const update = app.state.user.faces[0] || image && (image.id || image._id);
        AppUtils.uploadImage(app, data, 'add-face', onError, onSuccess, update)
    }

    _retrievePin() {
        const {app} = this.props.screenProps;
        try {
            AsyncStorage.getItem(app.pinKey, (err, pin) => {
                if (pin) {
                    this.setState({pin})
                    console.log('ScreenLock.fetchPin. ****** ===>')
                }
            })
        } catch (e) {
            console.log('ScreenLock.fetchPin.err ===>', e)
        }
    }

    _verify() {
        const {app} = this.props.screenProps
        const {navigation} = this.props
        const url = `${app.serverUrl}/mobile/v1/activate-identity`
        const headers = app.requestHeaders()
        const body = JSON.stringify({
            deviceInfo: app.deviceInfo,
            simData: app.simData,
            location: app.location
        })
        app.showActicity()
        fetch(url, {
            method: 'POST',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            }), body
        }).then(app.responseToJson(url)).then((responseData) => {
            app.hideActicity()
            if (responseData.success) {
                const user = _.extend(app.state.user, responseData.item)
                const token = user.token;
                const onUserChange = () => {
                    const userObj = _.extend({}, user, {token})
                    app.onUserChanged(userObj)
                    _.defer(() => {
                        if (user.hasIdentity && this.state.pin) {
                            app.replaceScreen(navigation, 'home')
                        } else {
                            app.replaceScreen(navigation, 'auth-pin')
                        }
                    })
                };
                AuthUtils.fetchToken(app, token, onUserChange)
            } else {
                Alert.alert('Attention', responseData.message + ", logging out")
                AuthUtils.logout(app);
            }
        }).catch((err) => {
            app.hideActicity()
            console.log('IdentityView._onActivateDevice.error ====> ', err)
        })
    }

    renderFooter() {
        const {app} = this.props.screenProps;
        const {user} = app.state;
        const shouldVerify = user.faces[0] || this.state.image
        if (shouldVerify) {
            return (
                <TouchableOpacity
                    style={styles.actionButton}
                    onPress={this._verify.bind(this)}
                >
                    <Text style={styles.actionText}>Verify</Text>
                </TouchableOpacity>
            )
        }
        return (
            <TouchableOpacity
                style={styles.actionButton}
                onPress={this._continueHandler.bind(this)}
            >
                <Text style={styles.actionText}>Continue</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const {app} = this.props.screenProps;
        const {user} = app.state;
        const {height, width} = Dimensions.get('screen');
        const imgWidth = (width * 3 / 4) - 100;
        const imageId = this.state.image ? this.state.image._id : user.faces[0]
        if (this.state.isShowingCamera) {
            return (
                <Camera closeCamera={this._onCloseCamera.bind(this)}
                        uploadImage={this._onUploadImage.bind(this)}/>
            )
        }
         const {textBold, actionButton} = AppStyles
        return (
            <View style={AppStyles.container}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity
                        onPress={()=>AuthUtils.logout(app)}
                        style={[{padding: 10}]}
                    >
                        <Text style={_.extend({}, textBold, {color: AppColors.primary})}>
                            Sign out
                        </Text>
                    </TouchableOpacity>
                </View>

                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.heading}>Welcome</Text>
                    {imageId ?
                        <TouchableOpacity
                            onPress={() => this.setState({isShowingCamera: true})}
                            style={{
                                width: imgWidth,
                                height: imgWidth, borderWidth: 1 / PixelRatio.get(), borderColor: AppColors.gray,
                                borderRadius: imgWidth / 2, marginRight: 10,
                            }}
                        >
                            <CachedImage
                                source={{uri: `${app.serverUrl}/images/${imageId}`}}
                                key={Date.now()}
                                style={{
                                    width: imgWidth,
                                    height: imgWidth,
                                    borderRadius: imgWidth / 2,
                                }}
                            />
                        </TouchableOpacity> :
                        <Icon name="ios-contact-outline" style={{fontSize: 80}}/>
                    }
                    <Text style={styles.text}>A face is needed</Text>
                </View>
                {this.renderFooter()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    actionButton: _.extend({}, AppStyles.actionButton, {margin: 10}),
    actionText: _.extend({}, AppStyles.textBold, {color: AppColors.white}),
    heading: _.extend({}, AppStyles.textBold, {fontSize: 18,}),
    text: _.extend({}, AppStyles.text)
});