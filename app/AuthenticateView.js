import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView, Alert,
    PixelRatio, Keyboard, AsyncStorage,
    StyleSheet, InteractionManager
} from 'react-native';
import _ from 'lodash';
import validate from 'validate.js'
import PhoneInput from 'react-native-phone-input'

import {AppStyles, AppColors} from './AppStyling';
import FloatLabelTextInput from './components/textfield/FloatLabelTextInput';
import FormField from './components/formfield/FormField';

import AuthUtils from './utils/AuthUtils'
import QuickVerification from "./QuickVerification";

const SIGN_UP = 'singup';
const LOG_IN = 'login';
const RESET_PWD = 'resetpassword';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                username: null,
                password: null
            },
            authFailed: false,
            errors: {}
        }
    }

    _onLogin() {
        if (this.validateForm()) {
            const {app, navigation} = this.props

            const {form} = this.state
            const url = `${app.serverUrl}/mobile/v1/auth/login`;
            const headers = app.requestHeaders();
            app.showActicity()
            fetch(url, {
                method: 'POST',
                headers: _.extend({}, headers, {
                    'Accept': 'application/json', 'Content-type': 'application/json'
                }), body: JSON.stringify(form)
            }).then(app.responseToJson(url)).then((respData) => {
                app.hideActicity()
                if (respData.success) {
                    AuthUtils.loginSuccessful(app, navigation, respData.item, respData.token)
                } else {
                    this.setState({authFailed: true, authFailedMsg: respData.message})
                }
            }).catch((err) => {
                app.hideActicity()
                console.log('Authenitcate._onLogin err ---->', err)
            })
        }
    }

    validateForm() {
        const constraints = {
            username: {
                presence: true, email: true,
            },
            password: {
                presence: true,
            }
        }
        const errors = validate(this.state.form, constraints) || {};
        this.setState({errors})
        return _.isEmpty(errors)
    }

    renderAuthFailed() {
        const failed = this.state.authFailed
        const {textBold} = AppStyles
        return (
            <View style={{alignItems: 'center'}}>
                { failed ?
                    <Text style={_.extend({}, textBold, {fontSize: 12, color: AppColors.danger})}>
                        {this.state.authFailedMsg}
                    </Text>
                    : null
                }
            </View>
        )
    }

    render() {
        const {app, onToggle} = this.props
        const {form, errors} = this.state
        const {textBold, actionButton} = AppStyles
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
            }}>
                {this.renderAuthFailed()}
                <FormField id="usernameTextInput" error={errors.username}>
                    <FloatLabelTextInput
                        ref="usernameTextInput"
                        label={"Email"}
                        value={form.username}
                        onChangeText={(text) => {
                            this.setState({form: _.extend({}, form, {username: text.trim()})})
                        }}
                        onSubmitEditing={() => this.refs.passwordTextInput.focus()}
                    />
                </FormField>
                <FormField id="passwordTextInput" error={errors.password}>
                    <FloatLabelTextInput
                        ref={'passwordTextInput'}
                        label={"Password"} type
                        value={form.password} secureTextEntry={true}
                        onChangeText={(text) => this.setState({form: _.extend({}, form, {password: text})})}
                        onSubmitEditing={() => {
                        }}
                    />
                </FormField>
                <TouchableOpacity
                    onPress={this._onLogin.bind(this)}
                    style={[actionButton, {marginTop: 10}]}
                >
                    <Text style={_.extend({}, textBold, {color: AppColors.white})}>
                        Log in
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onToggle(RESET_PWD)}
                    style={{marginTop: 10, padding: 3, alignSelf: 'center'}}
                >
                    <Text style={_.extend({}, textBold, {fontSize: 11})}>
                        Forgotten password?
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
class Signup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                name: null,
                lastname: null,
                phone: null,
                email: null,
                password: null
            },
            errors: {},
            countryCode: null
        }
    }

    _onSignup() {
        if (this.validateForm()) {
            const {form, countryCode} = this.state;
            console.log('phone ===>',_.extend({}, form,{phone: countryCode + form.phone}))
            this.props.onShowQuickVerification(_.extend({}, form,{phone: "+"+ countryCode + form.phone}))
        }
    }

    validateForm() {
        const {form} =this.state
        const constraints = {
            name: {presence: true},
            lastname: {presence: true},
            phone: {
                presence: true,
                format: {
                    pattern: /^\d{9}$/,
                    message: "number invalid, " + (form.phone && form.phone.length > 9 ? "too many digits" : " not enough digits")
                },
            },
            email: {presence: true, email: true},
            password: {
                presence: true,
                length: {
                    minimum: 6,
                    message: "must be at least 6 characters"
                }
            }
        };
        const errors = validate(form, constraints) || {}
        this.setState({errors});
        return _.isEmpty(errors)
    }

    renderCountryButton() {
        const {app} = this.props;
        const {deviceInfo, simData} =  app
        const country= simData && simData.countryCode || deviceInfo.country;
        const {countryCode, form} = this.state
        const setRef = (ref) => {
            if (ref) {
                this.phone = ref
                const code = ref.getCountryCode();
                if (code !== countryCode) {
                    this.setState({countryCode: code})
                }
            }
        };
        return (
            <PhoneInput
                ref={setRef} hideTextEditor={true}
                onSelectCountry={(country) => {
                    const code = this.phone.getCountryCode()
                    this.setState({countryCode: code})
                }}
                initialCountry={country && country.toLowerCase()}
            />
        )
    }

    render() {
        const {app} = this.props
        const {countryCode, errors, form} = this.state
        const {textBold, actionButton} = AppStyles
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
            }}>
                <FormField error={errors.name}>
                    <FloatLabelTextInput
                        ref="nameTextInput"
                        label={"Name"}
                        value={form.name}
                        onChangeText={(text) => {
                            this.setState({form: _.extend({}, form, {name: text})})
                        }}
                        onSubmitEditing={() => this.refs.lastnameTextInput.focus()}
                    />
                </FormField>

                <FormField error={errors.lastname}>
                    <FloatLabelTextInput
                        ref={'lastnameTextInput'}
                        label={"Lastname"}
                        value={form.lastname}
                        onChangeText={(text) => this.setState({form: _.extend({}, form, {lastname: text})})}
                        onSubmitEditing={() => this.refs.cellTextInput.focus()}
                    />
                </FormField>
                <FormField error={errors.phone}>
                    <FloatLabelTextInput
                        ref={'cellTextInput'}
                        leftButton={() => {
                            return countryCode ?
                                <Text style={_.extend({}, AppStyles.textBold)}>+{countryCode}</Text> : null
                        }}
                        rightButton={this.renderCountryButton.bind(this)}
                        placeholder={countryCode == 27 ? "711742025" : ""}
                        label={"Phone"} labelFixed={true}
                        value={form.phone}
                        keyboardType={'phone-pad'}
                        onChangeText={(text) => {
                            this.setState({form: _.extend({}, form, {phone: text})})
                        }}
                        onSubmitEditing={() => this.refs.emailTextInput.focus()}
                    />
                </FormField>
                <FormField error={errors.email}>
                    <FloatLabelTextInput
                        ref={'emailTextInput'}
                        label={"Email"}
                        value={form.email}
                        keyboardType={'email-address'}
                        onChangeText={(text) => this.setState({form: _.extend({}, form, {email: text.trim()})})}
                        onSubmitEditing={() => this.refs.passwordTextInput.focus()}
                    />
                </FormField>
                <FormField error={errors.password}>
                    <FloatLabelTextInput
                        ref={'passwordTextInput'}
                        label={"Password"}
                        value={form.password} secureTextEntry={true}
                        onChangeText={(text) => this.setState({form: _.extend({}, form, {password: text})})}
                        onSubmitEditing={() => Keyboard.dismiss()}
                    />
                </FormField>
                <TouchableOpacity
                    onPress={this._onSignup.bind(this)}
                    style={[actionButton, {marginTop: 10}]}
                >
                    <Text style={_.extend({}, textBold, {color: AppColors.white})}>
                        Sign up
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
class Resetpassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                email: null
            },
            errors: {}
        }
    }

    _onResetPassword() {
        if (this._validateForm()) {
            const {app} = this.props
            const url = `${app.serverUrl}/mobile/v1/auth/reset`
            const {form} = this.state
            const {headers} = app.requestHeaders()
            app.showActicity()
            fetch(url, {
                method: 'POST',
                headers: _.extend({}, headers, {
                    'Accept': 'application/json', 'Content-type': 'application/json'
                }), body: JSON.stringify(form)
            }).then(app.responseToJson(url)).then((responseData) => {
                app.hideActicity()
                if (responseData.success) {
                    Alert('Password request sents')
                }
            }).catch((err) => {
                app.hideActicity()
                console.log('Authentication._onResetPassword err ===> ', err)
            })
        }
    }

    _validateForm() {
        const constraints = {
            email: {presence: true, email: true}
        };
        const errors = validate(this.state.form, constraints) || {};
        this.setState({errors});
        return _.isEmpty(errors);
    }

    render() {
        const {form, errors} = this.state;
        const {textBold, actionButton} = AppStyles;
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
            }}>
                <FormField error={errors.email}>
                    <FloatLabelTextInput
                        ref="emailTextInput"
                        label={"Email"}
                        value={form.email}
                        onChangeText={(text) => {
                            this.setState({form: _.extend({}, form, {email: text.trim()})})
                        }}
                    />
                </FormField>
                <TouchableOpacity
                    onPress={this._onResetPassword.bind(this)}
                    style={[actionButton, {marginTop: 10}]}
                >
                    <Text style={_.extend({}, textBold, {color: AppColors.white})}>
                        Reset Password
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default class Authenticate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //  currentForm: SIGN_UP
            showQuickVerification: false
        }
    }

    static navigationOptions = (props) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        this._checkHasHadUser()
    }

    _checkHasHadUser() {
        const {app} = this.props.screenProps
        AsyncStorage.getItem(app.hasHadUserKey, (err, hasHadUser) => {
            if (hasHadUser) {
                this.setState({currentForm: LOG_IN})
            } else {
                this.setState({currentForm: SIGN_UP})
            }
        })
    }

    _onToggle(formName) {
        this.setState({currentForm: formName})
    }

    _renderToggler() {
        const {currentForm} = this.state
        const {textBold, actionButton} = AppStyles
        if (currentForm !== LOG_IN) {
            return (
                <TouchableOpacity
                    style={_.extend({}, actionButton, {alignSelf: 'flex-end', padding: 3,})}
                    onPress={() => this._onToggle(LOG_IN)}
                >
                    <Text style={_.extend({}, textBold, {color: AppColors.white})}>Log in</Text>
                </TouchableOpacity>
            )
        }
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {alignSelf: 'flex-end', padding: 3,})}
                onPress={() => this._onToggle(SIGN_UP)}
            >
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Sign up</Text>
            </TouchableOpacity>
        )
    }

    _onShowQuickVerification(form) {
        this.setState({showQuickVerification: true, form})
    }

    _onCloseQuickVerification() {
        this.setState({showQuickVerification: false})
    }

    render() {
        const {app} = this.props.screenProps;
        const {navigation} = this.props
        return (
            <View style={_.extend({}, AppStyles.container, {
                padding: 10
            })}>
                {this._renderToggler()}
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    {
                        (() => {
                            switch (this.state.currentForm) {
                                case SIGN_UP:
                                    return <Signup
                                        app={app}
                                        navigation={navigation}
                                        onShowQuickVerification={this._onShowQuickVerification.bind(this)}
                                    />
                                    break;
                                case LOG_IN:
                                    return <Login
                                        app={app} navigation={navigation}
                                        onToggle={this._onToggle.bind(this)}
                                    />
                                    break;
                                case RESET_PWD:
                                    return <Resetpassword app={app} navigation={navigation}/>
                                    break;
                                default:
                                    return null
                                    break;
                            }
                        })()
                    }
                </ScrollView>
                {this.state.showQuickVerification ?
                    <QuickVerification
                        app={app} navigation={navigation} form={this.state.form}
                        onClose={this._onCloseQuickVerification.bind(this)}
                        onSuccess={() => {
                            AuthUtils.setHasHadUser(app);
                            Alert.alert('Info', 'Sign up completed');
                            _.defer(() => {
                                this.setState({currentForm: LOG_IN})
                            })
                        }
                        }
                    />
                    : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});