import React from 'react'
import {
    AsyncStorage,
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Switch
} from 'react-native'
import _ from 'lodash'
import MenuButton from './components/navbar/MenuButton'
import {AppStyles, AppColors} from './AppStyling'
import Icon from 'react-native-vector-icons/Ionicons'
import Locking from './Modules'

export default class HomeView extends React.Component {
    constructor(props) {
        super(props)
        const {params} = props.navigation.state
        this.state = {
            navigation: props.navigation.headers,
            devices: []
        }
    }

    static navigationOptions = (props) => {
        return {
            title: 'Home',
            headerRight: <MenuButton {...props}/>,
            headerStyle: AppStyles.navbar
        }
    };

    componentWillMount() {
        this._getScreenLockSetting();
    }

    componentDidMount() {
        const {app} = this.props.screenProps;
        app.setNav(this.props.navigation)
    }

    _getScreenLockSetting() {
        const {app} = this.props.screenProps
        try {
            AsyncStorage.getItem(app.screenLockKey, (err, setting) => {
                if (setting) {
                    this.setState({enabled: setting == 'true'})
                } else {
                    this.setState({enabled: false})
                }
            });
        } catch (e) {
            console.log('HomeView._getScreenLockSetting.err ===> ', e)
        }
    }

    _screenLockEnabled() {
        this.setState((prevState, props) => {
            const enabled = !prevState.enabled;
            enabled ? Locking.enable() : Locking.disable();
            const {app} = this.props.screenProps
            try {
                AsyncStorage.setItem(app.screenLockKey, JSON.stringify(enabled));
            } catch (e) {
                console.log('HomeView.componentWillUnmount.saveItem err ===>', e)
            }
            return {
                enabled
            }
        })
    }

    renderFooter() {
        const {actionButton, actionIcon, textBold} = AppStyles;
        const {enabled} = this.state

        return (
            <TouchableOpacity
                disabled={!enabled}
                style={_.extend({}, actionButton, {
                    margin: 10, backgroundColor: enabled ? AppColors.primary : AppColors.gray
                })}
                onPress={() => Locking.lock()}
            >
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Lock Now</Text>
                <Icon style={actionIcon} name="ios-lock"/>
            </TouchableOpacity>
        )
    }

    render() {
        const {textBold} = AppStyles;
        const {app} = this.props.screenProps;
        const device = app.deviceInfo;
        const sim = app.simData;
        const renderText = (title) => (
            !title ? null : <Text
                style={_.extend({}, textBold, {textAlign: 'center'})}>
                {title}
            </Text>
        )
        return (
            <View style={AppStyles.container}>
                <View style={{flex: 1, padding: 10}}>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                        <Text style={_.extend({}, textBold)}>Enable lock</Text>
                        <Switch
                            onValueChange={this._screenLockEnabled.bind(this)}
                            value={this.state.enabled}
                        />
                    </View>
                    <View style={styles.renderItem}>
                        <Icon name="ios-phone-portrait-outline" size={100} style={{alignSelf: 'center'}}/>
                        {device ?
                            <View style={{marginBottom: 10, flexDirection: 'column'}}>
                                {renderText(device.brand)}
                                {renderText(device.deviceId)}
                                {renderText(device.timezone)}
                                {renderText(device.country)}
                            </View> : null
                        }
                        <Icon name="ios-stats-outline" size={20} style={{alignSelf: 'center'}}/>
                        {sim ?
                            <View style={{marginBottom: 10, flexDirection: 'column'}}>
                                {renderText(sim.carrier)}
                                {renderText(sim.phoneNumber)}
                                {renderText(sim.countryCode)}
                            </View> :
                            <Text style={_.extend({}, textBold, {textAlign: 'center'})}>No Sim Info</Text>
                        }
                    </View>
                </View>
                {this.renderFooter()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
        renderItem: {
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10, flex: 1,
            padding: 10,
            backgroundColor: AppColors.white
        }
    }
)


