import {StackNavigator} from 'react-navigation';

import Authenticate from './AuthenticateView';
import AppMenu from './AppMenu'
import DeviceList from './device/DeviceList'
import DeviceView from './device/DeviceView'
import Home from './HomeView'
import ProfileView from './profile/ProfileView'
import ScreenLock from './ScreenLock'
import SplashView from './SplashView'
import SuspectListView from './device/SuspectListView'
import WelcomeView from './WelcomeView'
import PinSetup from './device/PinSetup'

const initialRouteName = 'splash'//'auth';

function getCurrentParams(state) {
    if (state.routes) {
        return getCurrentParams(state.routes[state.index]);
    }
    return state.params || {};
}

const Routes = StackNavigator(
    {
        'splash': {
            screen: SplashView,
        },
        'auth': {
            screen: Authenticate,
        },
        'auth-pin': {
            screen: PinSetup
        },
        'device-view': {
            screen: DeviceView
        },
        'devices': {
            screen: DeviceList,
        },
        'home': {
            screen: Home,
        },
        'menu': {
            screen: AppMenu,
        },
        'profile/profile-view': {
            screen: ProfileView
        },
        'screen-lock':{
            screen: ScreenLock
        },
        'suspects': {
            screen: SuspectListView
        },
        'welcome': {
            screen: WelcomeView
        }
    },
    {
        initialRouteName
    }
);
module.exports = {Routes}