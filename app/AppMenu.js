import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet, PixelRatio} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import _ from 'lodash'
import AuthUtils from './utils/AuthUtils'
import MenuButton from './components/navbar/MenuButton'
import {AppStyles, AppColors} from './AppStyling'

export default class AppMenu extends React.Component {
    static navigationOptions = (props) => {
        return {
            title: 'Menu',
        }
    }

    _onSignOut() {
        const {app} = this.props.screenProps
        AuthUtils.logout(app)
    }

    _onViewProfile() {
        const {navigation} = this.props
        navigation.push('profile/profile-view')
    }

    _onViewMyDevices() {
        const {navigation} = this.props
        navigation.push('devices')
    }

    renderFooter() {
        const {actionButton, actionIcon, textBold} = AppStyles;
        const navigate = () => {
            const {app} = this.props.screenProps
            const {navigation} = this.props
            app.replaceScreen(navigation, 'home')
        }
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {
                    margin: 10,
                })}
                onPress={navigate}>
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Home</Text>
                <Icon style={actionIcon} name="ios-home"/>
            </TouchableOpacity>
        )
    }

    renderHeader() {
        const {textBold} = AppStyles
        const {app} = this.props.screenProps;
        return (
            <View style={{padding: 10, marginVertical: 10, alignItems: 'center'}}>
                <Text style={_.extend({}, textBold, {fontSize: 18, marginBottom: 3})}>Teleport</Text>
                <Text style={_.extend({}, textBold)}>
                    {`App version: ${app.deviceInfo.appVersion}`}
                </Text>
                <Text style={_.extend({}, textBold)}>
                    {`Build number: ${app.deviceInfo.buildNumber}`}
                </Text>
            </View>
        )
    }

    render() {
        const {textBold} = AppStyles
        return (
            <View style={AppStyles.container}>
                {this.renderHeader()}
                <View style={{flex: 1}}>
                    <TouchableOpacity
                        style={[styles.itemContainer, {
                            borderTopColor: AppColors.gray,
                            borderTopWidth: 1 / PixelRatio.get()
                        }]}
                        onPress={this._onViewProfile.bind(this)}
                    >
                        <Icon name={'ios-contact-outline'} style={styles.icon}/>
                        <Text style={_.extend({}, textBold, {marginLeft: 5, flex: 1})}>Profile</Text>
                        <Icon name="ios-arrow-forward-outline"/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemContainer}
                        onPress={this._onViewMyDevices.bind(this)}
                    >
                        <Icon name={'ios-phone-portrait-outline'} style={styles.icon}/>
                        <Text style={_.extend({}, textBold, {marginLeft: 5, flex: 1})}>My devices</Text>
                        <Icon name="ios-arrow-forward-outline"/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.itemContainer}
                        onPress={this._onSignOut.bind(this)}
                    >
                        <Icon name={'ios-log-out-outline'} style={styles.icon}/>
                        <Text style={_.extend({}, textBold, {marginLeft: 5})}>Sign out</Text>
                    </TouchableOpacity>
                </View>

                {this.renderFooter()}

            </View>
        )
    }
}
const styles = StyleSheet.create({
    icon: {
        fontSize: 17,
        textAlign: 'center',
        width: 17
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 12,
        borderBottomColor: AppColors.gray,
        borderBottomWidth: 1 / PixelRatio.get()
    }
})