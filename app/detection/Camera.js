import React, {Component} from 'react'
import {
    View,
    Text, Alert,
    TouchableOpacity,
    StyleSheet, Platform,
    InteractionManager
} from 'react-native'
import {RNCamera, FaceDetector,} from 'react-native-camera'
import {AppStyles, UITheme, AppColors} from '../AppStyling'
import Icon from 'react-native-vector-icons/Ionicons'
import _ from 'lodash'

const DESIRED_RATIO = "4:3";
export default class Camera extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            hasFace: false,
            hideCapture: props.hideCapture,
            detectsFace: false,
        };
        this.takePictureAsyncDebounce = _.debounce(
            this._takePicture, 3000, {'leading': true, 'trailing': false}
        );
    }
    componentDidMount(){
        setTimeout(()=>{
           if(!this.state.detectsFace) {
               this.setState({hideCapture: false})
           }
        }, 5000)
    }

    prepareRatio = async () => {
        if (Platform.OS === 'android' && this.cam) {
            const ratios = await this.camera.getSupportedRatiosAsync();
            const ratio = ratios.find((ratio) => ratio === DESIRED_RATIO) || ratios[ratios.length - 1];
            this.setState({ratio});
        }
    }

    _takePicture = async function () {
        //console.log('Camera._takePicture ***************')
        const {closeCamera, uploadImage} = this.props
        if (this.camera) {
            const options = {quality: 0.5, base64: true, fixOrientation: true};
            const data = await this.camera.takePictureAsync(options)
            console.log('Camera._takePicture ***************',_.keys(data))

            uploadImage && uploadImage(data)
            closeCamera && closeCamera()
        }
    };

    onFacesDetected(data) {
        console.log('Camrea.onFacesDetected ===>', _.pick(data));
        if(!this.state.detectsFace){
            this.setState({detectsFace: true});
        }
        const {hasFace} = this.state
        const typeFace = (data.type === "face");
        if (typeFace && data.faces.length === 1) {
            if (this.props.hideCapture && !hasFace) {
                this.takePictureAsyncDebounce()
            }
            if(!hasFace){
                this.setState({hasFace: true})
            }
        } else if (typeFace && hasFace) {
            this.setState({hasFace: false})
        }
    }

    resetState() {
        this.setState({hasFace: false})
    }

    render() {
        const {hasFace,hideCapture,detectsFace} = this.state
        const enableCapturing = hasFace || !detectsFace;
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    onCameraReady={this.prepareRatio}
                    ratio={this.state.ratio}
                    type={RNCamera.Constants.Type.front}
                    flashMode={RNCamera.Constants.FlashMode.auto}
                    onFacesDetected={this.onFacesDetected.bind(this)}
                    faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.accurate}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                />
                {hideCapture ? null :
                    <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
                        <TouchableOpacity
                            disabled={!enableCapturing}
                            onPress={this._takePicture.bind(this)}
                            style={styles.capture}
                        >
                            <View style={{
                                flex: 1,
                                padding: 13,
                                paddingHorizontal: 18,
                                borderRadius: 100,
                                backgroundColor: enableCapturing ? AppColors.primary : 'transparent'
                            }}>
                                <Icon name={'ios-camera-outline'} color={AppColors.white}
                                      style={{fontSize: 35}}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,// height: UITheme.height, width: UITheme.width,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0, position: 'absolute', bottom: 0,
        backgroundColor: AppColors.transparent,
        borderColor: AppColors.white,
        borderWidth: 2,
        borderRadius: 100,
        // padding: 13,
        elevation: 10,
        //paddingHorizontal: 18,
        alignSelf: 'center',
        margin: 10
    }
})