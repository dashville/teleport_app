import ReconnectingWebSocket from 'reconnecting-websocket'
import _ from 'lodash'

class ReconnectingSocket {
    constructor(props) {
        this.props = props
        this.register = this._registerSocket.bind(this)
        this.unregister = this._unregisterSocket.bind(this)
        this.updatePosition = this._updatePosition.bind(this)
    }

    _registerSocket() {
        const app = this.props.app
        this.websocket = new ReconnectingWebSocket(app.wsUrl)

        this.websocket.onopen = (ws) => {
            if (ws.readyState === ws.OPEN) {
                const handshakeData = {
                    'command-id': 'handshake',
                    data: app.requestHeaders()
                }
                this.websocket.send(JSON.stringify(handshakeData))
                app.getCurrentLocation()
            }
        }

        this.websocket.onmessage = (ws) => {
            this._handleIncomingMessage(JSON.parse(ws.data))
        }
    }

    _updatePosition(position) {
        if (position && position.coords) {
            const app = this.props.app
            app.saveLocation(position)
            const data = {
                'command-id': 'update-location',
                data: _.extend({}, position, {uniqueId: app.deviceInfo.uniqueId})
            }
            if (this.websocket.readyState === this.websocket.OPEN) {
                try {
                    this.websocket.send(JSON.stringify(data))
                } catch (err) {
                    console.log('ReconnectingSocket._updatePosition.err ==> ', err)
                }
            } else {
                console.log(
                    'ReconnectingSocket._updatePosition not open *******************'
                )
            }
        }
    }

        _handleIncomingMessage(data)
        {
            //console.log('ReconnectingSocket._handleIncomingMessage ===>',_.pick(data,['success','command-id' ]))
        }

        _unregisterSocket()
        {
            if (this.websocket) {
                try {
                    this.websocket.close()
                } catch (err) {
                    console.log('ReconnectingSocket._unregisterSocket.err ====>', err)
                }
            }
        }
    }

    module
.
    exports = ReconnectingSocket