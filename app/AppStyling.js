import React from "react";
import {Dimensions} from 'react-native'

let {height, width} = Dimensions.get('screen')
const UITheme = {
    width,
    height
}

const AppColors = {
    //rimary: '#32CD32',
    primary: '#4682B4',
    white: '#ffffff',
    black: '#000000',
    info: '#3393FF',
    danger: '#b20000',
    tintedGray: 'rgba(0,0,0,0.1)',
    transparent: 'transparent',
    gray: '#A9A9A9',
    overlay:'rgba( 0, 0, 0, 0.5)',
    overlayDark: 'rgba( 0, 0, 0, 0.8)'
}

const AppStyles = {
    actionIcon: {
        color: AppColors.white,
        fontSize: 16,
        marginHorizontal: 5,
    },
    container: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: AppColors.white,
        elevation: 0.5,
    },
    text: {
        fontFamily: 'ClanOT-Book',
        color: AppColors.black,
    },
    textBold: {
        fontFamily: 'ClanOT-News',
        color: AppColors.black,
    },
    actionButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 3, elevation: 0.5,
        backgroundColor: AppColors.primary,
    },
    navbar: {
        paddingHorizontal: 2,
    }
}


module.exports = {AppStyles, AppColors, UITheme}