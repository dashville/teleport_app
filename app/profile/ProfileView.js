import React from 'react'
import {
    Alert,
    Dimensions,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import _ from 'lodash';
import moment from 'moment';
import validate from 'validate.js'
import MenuButton from '../components/navbar/MenuButton'

import {AppColors, AppStyles,} from '../AppStyling';

import FormField from '../components/formfield/FormField';
import FloatLabelTextInput from '../components/textfield/FloatLabelTextInput';
import Icon from 'react-native-vector-icons/Ionicons'
import {CachedImage}from 'react-native-cached-image';
import AppUtils from '../utils/AppUtils'
import PhoneInput from 'react-native-phone-input'

export default class ProfileView extends React.Component {
    constructor(props) {
        super(props);
        const {user} = props.screenProps.app.state;
        const form = _.pick(user, ["name", "lastname", "email", "phone", "phone-alt"]);
        this.state = {
            form,
            errors: {}
        }
    }

    static navigationOptions = (props) => {
        return {
            title: 'Profile',
            headerRight: <MenuButton {...props}/>,
            headerStyle: AppStyles.navbar
        }
    }

    _updateHandler() {
        const {form} = this.state
        if (this.validateForm(form)) {
            const {app} = this.props.screenProps;
            const headers = app.requestHeaders()
            const url = `${app.serverUrl}/mobile/v1/profile/update`;
            fetch(url, {
                method: "PATCH",
                headers: _.extend({}, headers, {
                    'Accept': 'application/json', 'Content-Type': 'application/json'
                }),
                body: JSON.stringify(form)
            }).then(app.responseToJson(url)).then((responseData) => {
                if (responseData.success) {
                    const item = responseData.item
                    app.onUserChanged(_.extend({}, app.state.user, item));
                    Alert.alert("Info", "Profile updated");
                    const {navigation} = this.props
                    navigation.pop()
                } else {
                    Alert.alert("Error", responseData.message)
                }
            }).catch((e) => {
                console.log('UserDetails._handleUpdate.err ======>', e)
            })
        }
    }

    validateForm(form) {
        const isValidCountryCode = (number)=>{
            return number ? number.substr(1,2) == '27' : true
        }
        const constraints = {
            name: {presence: true},
            lastname: {presence: true},
            email: {presence: true, email: true},
            phone: {
                presence: true,
                format: {
                    pattern: /^\+27[0-9]{9}$/,
                    message: "number invalid, " + (isValidCountryCode(form.phone) ? "" : "+27 is required, ")+ (form.phone && form.phone.length > 13 ? "too many digits" : " not enough digits")
                 }
            },
            'phone-alt': {
                presence: false,
                format: {
                    pattern: /^\+27[0-9]{9}$/,
                    message: "number invalid, " + (isValidCountryCode(form['phone-alt']) ? "": "+27 is required, ") + (form['phone-alt'] && form['phone-alt'].length > 13 ? "too many digits" : " not enough digits")
                 }
            }
        };
        const errors = validate(form, constraints) || {}
        this.setState({errors});
        return _.isEmpty(errors)
    }

    renderFooter() {
        const {textBold, actionButton, actionIcon} = AppStyles;
        return (
            <TouchableOpacity
                onPress={this._updateHandler.bind(this)}
                style={[actionButton, {marginTop: 10}]}
            >
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>
                    Update Profile
                </Text>
                <Icon name="ios-send" style={actionIcon}/>
            </TouchableOpacity>
        )
    }
    renderCountryButton() {
        const {app} = this.props;
        const {country} = app.deviceInfo;
        const {countryCode, form} = this.state
        const setRef = (ref) => {
            if (ref) {
                this.phone = ref
                const code = ref.getCountryCode();
                if (code !== countryCode) {
                    this.setState({countryCode: code}, () => {
                        console.log('Auth.form ==> ', this.state.form)
                    })
                }
            }
        };
        return (
            <PhoneInput
                ref={setRef} hideTextEditor={true}
                onSelectCountry={(country) => {
                    const code = this.phone.getCountryCode()
                    this.setState({countryCode: code})
                }}
                initialCountry={country.toLowerCase()}
            />
        )
    }

    renderHeader() {
        const {app} = this.props.screenProps;
        const {user} = app.state;
        const {width} = Dimensions.get('screen');
        const imgWidth = (width * 3 / 4) - 200;
        const {textBold} = AppStyles;
        return (
            <View style={styles.header}>
                <CachedImage
                    source={{uri: `${app.serverUrl}/images/${user.faces[0]}`}}
                    style={{
                        width: imgWidth,
                        height: imgWidth,
                        borderRadius: imgWidth / 2, marginRight: 10,
                    }}
                />
                <View>
                    <Text style={textBold}>
                        {`Username: ${user.username}`}
                    </Text>
                    <Text style={textBold}>
                        {`Joined on: ${moment(Number(AppUtils.toLocalTime(user.createdOn))).format('DD/MM/YYYY')}`}
                    </Text>
                </View>
            </View>
        )
    }

    render() {
        const {form, errors} = this.state;
        const {container} = AppStyles;
        return (
            <View style={container}>
                <ScrollView contentContainerStyle={{flexGrow: 1, padding: 10}}>
                    {this.renderHeader()}
                    <View style={{flex: 1}}>
                        <FormField error={errors.name}>
                            <FloatLabelTextInput
                                ref="nameTextInput"
                                label={"Name"}
                                value={form.name}
                                onChangeText={(text) => {
                                    text = text.trim().length === 0 ? null : text
                                    this.setState({form: _.extend({}, form, {name: text})})
                                }}
                                onSubmitEditing={() => this.refs.lastnameTextInput.focus()}
                            />
                        </FormField>

                        <FormField error={errors.lastname}>
                            <FloatLabelTextInput
                                ref={'lastnameTextInput'}
                                label={"Lastname"}
                                value={form.lastname}
                                onChangeText={(text) => {
                                    text = text.trim().length === 0 ? null : text
                                    this.setState({form: _.extend({}, form, {lastname: text})})
                                }}
                                onSubmitEditing={() => this.refs.emailTextInput.focus()}
                            />
                        </FormField>
                        <FormField error={errors.email}>
                            <FloatLabelTextInput
                                ref={'emailTextInput'}
                                label={"Email"}
                                value={form.email}
                                keyboardType={'email-address'}
                                onChangeText={(text) => {
                                    text = text.trim().length === 0 ? null : text
                                    this.setState({form: _.extend({}, form, {email: text.trim()})})
                                }}
                                onSubmitEditing={() => this.refs.cellTextInput.focus()}
                            />
                        </FormField>

                        <FormField error={errors.phone}>
                            <FloatLabelTextInput
                                ref={'cellTextInput'}
                                label={"Phone (+27)"}
                                value={form.phone}
                                keyboardType={'phone-pad'}
                                onChangeText={(text) => {
                                    text = text.trim().length === 0 ? null : text
                                    this.setState({form: _.extend({}, form, {phone: text})})
                                }}
                                onSubmitEditing={() => this.refs.phoneAltTextInput.focus()}
                            />
                        </FormField>
                        <FormField error={errors['phone-alt']}>
                            <FloatLabelTextInput
                                ref={'phoneAltTextInput'}
                                label={"Phone Alt (+27)"}
                                value={form['phone-alt']}
                                keyboardType={'phone-pad'}
                                onChangeText={(text) => {
                                    text = text.trim().length === 0 ? null : text
                                    this.setState({form: _.extend({}, form, {'phone-alt': text})})
                                }}
                                onSubmitEditing={() => {
                                }}
                            />
                        </FormField>
                    </View>
                    {this.renderFooter()}
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    header:{flexDirection: 'row', alignItems: 'center', marginBottom: 10}
})
