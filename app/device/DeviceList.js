import React from 'react';
import {
    FlatList,
    PixelRatio, InteractionManager,StyleSheet, Text, TextInput, TouchableOpacity, View, Keyboard
} from 'react-native';
import _ from 'lodash';
import MenuButton from '../components/navbar/MenuButton'
import Icon from 'react-native-vector-icons/Ionicons'

import {AppColors, AppStyles, UITheme} from "../AppStyling"

export default class DeviceList extends React.Component {
    state = {devices: []}
    static navigationOptions = (props) => {
        return {
            title: 'My Devices',
            headerRight: <MenuButton {...props}/>,
            headerStyle: AppStyles.navbar,
            searchText: ''
        }
    }

    _fetchDevices() {
        const {app} = this.props.screenProps
        const url = `${app.serverUrl}/mobile/v1/devices`
        const headers = app.requestHeaders()
        app.showActicity()
        fetch(url, {
            method: 'GET',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(url)).then((responseData) => {
            app.hideActicity()
            if (responseData.success) {
                this.devices = responseData.items
                this.setState({devices: responseData.items})
            }
        }).catch((err) => {
            app.hideActicity()
            console.log('HomeView._fetchDevices.err =====>', errr)
        })
    }

    renderItem(rowItem) {
        const {navigation} = this.props
        const device = rowItem.item
        const {width} = UITheme
        const iconWidth = (width * 3 / 4) - 218
        const renderLine = (value) => {
            if (!value) {
                return null
            }
            return <Text style={_.extend({}, AppStyles.textBold)}>{value}</Text>
        }
        return (
            <TouchableOpacity
                onPress={() => navigation.navigate('device-view', {device})}
                style={styles.renderItem}
            >
                <View
                    style={[styles.iconContainer, {width: iconWidth, height: iconWidth, borderRadius: iconWidth / 2}]}>
                    <Icon name="ios-phone-portrait-outline" size={40} style={{color: AppColors.white}}/>
                </View>
                <View style={styles.detailsContainer}>
                    {renderLine(device.brand)}
                    {renderLine(device.deviceId)}
                    {renderLine(device.timezone)}
                    {renderLine(device.country)}
                </View>
                <Icon name="ios-arrow-forward-outline" size={17}/>
            </TouchableOpacity>
        )
        //
    }

    componentDidMount() {
        this._fetchDevices();
    }

    renderFooter() {
        const {actionButton, actionIcon, textBold} = AppStyles;
        const navigate = () => {
            const {app} = this.props.screenProps
            const {navigation} = this.props
            app.replaceScreen(navigation, 'home')
        }
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {
                    margin: 10,
                })}
                onPress={navigate}>
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Home</Text>
                <Icon style={actionIcon} name="ios-home"/>
            </TouchableOpacity>
        )
    }

    handleSearch(text) {
        if (text) {
            const filteredDevices = _.filter(this.devices, (device) => {
                const brand = device.brand.toLowerCase()
                return (brand.includes(text.toLowerCase()))
            })
            this.setState({devices: filteredDevices, searchText: text.trim()})
        } else {
            this.setState({devices: this.devices, searchText: text.trim()})

        }
    }

    onClear = () => {
        Keyboard.dismiss()
        this.searchTextInput.setNativeProps({text: ''});
        this.setState({searchText: null, devices: this.devices})
    };

    renderSearchInput() {
        const {searchText} = this.state
        const {text} = AppStyles
        return (
            <View style={styles.searchContainer}>
                <View style={{marginHorizontal: 5}}>
                    <Icon name="ios-search-outline" size={25}/>
                </View>
                <TextInput
                    ref={(ref) => this.searchTextInput = ref}
                    style={_.extend({}, text, {flex: 1})}
                    value={searchText}
                    placeholder={"Search by brand..."}
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => {
                        this.handleSearch(text)
                    }}
                    onSubmitEditing={() => {
                        Keyboard.dismiss();
                    }}

                />
                <TouchableOpacity
                    onPress={this.onClear.bind(this)}
                    style={{marginHorizontal: 5}}>
                    <Icon name="ios-close-outline" size={25}/>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={AppStyles.container}>
                {this.renderSearchInput()}
                <FlatList
                    style={{padding: 10, flex: 1}}
                    data={this.state.devices}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => item._id}
                />
                {this.renderFooter()}
            </View>
        )

    }
}
const styles = StyleSheet.create({
        detailsContainer: {flex: 1, marginLeft: 10, flexWrap: "wrap"},
        iconContainer: {
            alignItems: 'center',
            backgroundColor: AppColors.overlay,
            borderColor: AppColors.gray,
            borderWidth: 1 / PixelRatio.get(),
            justifyContent: 'center',
        },
        renderItem: {
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: AppColors.gray,
            borderBottomWidth: 1 / PixelRatio.get(),
            backgroundColor: AppColors.white
        },
        searchContainer: {
            borderColor: AppColors.gray,
            borderWidth: 1,
            borderRadius: 3,
            alignItems: 'center',
            flexDirection: 'row',
            margin: 10,
        }
    }
)
