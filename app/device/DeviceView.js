import React from 'react';
import {
    FlatList,
    Image,
    PixelRatio,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    NativeModules
} from 'react-native';
import _ from 'lodash';
import {AppColors, AppStyles, UITheme} from '../AppStyling';
import Icon from 'react-native-vector-icons/Ionicons'
const Intent = NativeModules.IntentModuleAndroid;
import MenuButton from '../components/navbar/MenuButton'
import moment from 'moment'
import AppUtils from '../utils/AppUtils'
export default class DeviceView extends React.Component {
    state = {
        location: null,
        doneFetching: false
    };
    static navigationOptions = (props) => {
        return {
            title: 'Device',
            headerRight: <MenuButton {...props}/>,
            headerStyle: AppStyles.navbar
        }
    }

    componentDidMount() {
        this._fetchLocation()
    }

    _fetchLocation() {
        const {app} = this.props.screenProps;
        const {navigation} = this.props;
        const device = navigation.getParam('device');
        const headers = app.requestHeaders();
        const url = `${app.serverUrl}/mobile/v1/location/${device.uniqueId}`;
        app.showActicity();
        fetch(url, {
            method: "GET",
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(url)).then(responseData => {
            console.log('DeviceView._fetchLocation.responseData ===>', responseData)
            app.hideActicity();
            if (responseData.success) {
                this.setState({location: responseData.item, doneFetching: true})
            }
        }).catch(err => {
            app.hideActicity();
            this.setState({doneFetching: true})
            console.log('DeviceView._fetchLocation.err ===>', err)
        })
    }

    renderDetails() {
        const {navigation} = this.props
        const device = navigation.getParam('device');
        const sim = device.simData;
        const {location} = this.state
        const {textBold} = AppStyles;
        const renderLine = (title, value) => {
            if (!value) {
                return null
            }
            return <Text style={_.extend({}, textBold)}>{`${title} : ${value}`}</Text>
        }
        const formatDate = (tstamp) => {
            return moment(Number(AppUtils.toLocalTime(tstamp))).format('DD/MM/YYYY HH:mm')
        }
        return (
            <View
                style={styles.deviceInfoContainer}
            >
                <Icon name="ios-phone-portrait-outline" size={80} style={{alignSelf: 'center'}}/>
                <View style={styles.section}>
                    {renderLine('Brand', device.brand)}
                    {renderLine('Device Id', device.deviceId)}
                    {renderLine('Timezone', device.timezone)}
                    {renderLine('Country', device.country)}
                </View>

                <Icon name="ios-stats-outline" size={20} style={{alignSelf: 'center', color: 'green'}}/>
                {sim ?<View style={styles.section}>
                    {renderLine('Carrier', sim.carrier)}
                    {renderLine('Phone number', sim.phoneNumber)}
                    {renderLine('Country', sim.countryCode)}
                </View> : <Text style={_.extend({}, textBold)}>{"No Sim Info"}</Text>
                }

                {location ?
                    <View>
                        <Icon name="ios-locate-outline" size={20} style={{alignSelf: 'center',color: 'red'}}/>
                        <View style={{flexDirection: 'row', marginBottom: 10, alignItems: 'center'}}>
                            <View style={{flexDirection: 'column', flex: 1}}>
                                {renderLine('Location', location.formattedAddress)}
                                {renderLine('Last update', formatDate(location.updatedOn || location.createdOn))}
                            </View>
                            <TouchableOpacity
                                style={_.extend({}, AppStyles.actionButton, {padding: 3,})}
                                onPress={this._viewCurrentLocation.bind(this)}
                            >
                                <Text style={_.extend({}, textBold, {color: AppColors.white})}>View</Text>
                                <Icon name="ios-map" size={15} style={{color: AppColors.white, marginLeft: 3}}/>
                            </TouchableOpacity>
                        </View>
                    </View> : null}
            </View>
        )
    }

    renderHeader() {
        const {app} = this.props.screenProps;
        const {user} = app.state
        const {navigation} = this.props
        const device = navigation.getParam('device');
        const isMainDevice = user.mainDevice === device._id;
        const isCurrentDevice = device.uniqueId === app.deviceInfo.uniqueId;
        const renderLine = (hasType, type) => {
            const {textBold} = AppStyles;
            if (!hasType) {
                return null
            }
            return <Text style={_.extend({}, textBold)}>{type}</Text>
        }
        return (
            <View style={styles.header}>
                {renderLine(isMainDevice, 'Main Device')}
                {renderLine(isCurrentDevice, 'Current Device')}
            </View>
        )
    }

    _viewCurrentLocation() {
        const loc = this.state.location
        const {lat, lng} = loc.coords
        Intent.navigateTo(Number(lat), Number(lng), loc.formattedAddress)
    }

    _viewSuspects() {
        const {navigation} = this.props;
        const device = navigation.getParam('device');
        navigation.navigate('suspects', {device})
    }

    renderFooter() {
        const {actionButton, actionIcon, textBold} = AppStyles;
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {
                    margin: 10,
                })}
                onPress={this._viewSuspects.bind(this)}>
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Who Has My Device</Text>
            </TouchableOpacity>
        )
    }

    render() {
        if(!this.state.doneFetching){
            return null
        }
        const {navigation} = this.props
        const device = navigation.getParam('device');
        return (
            <View style={AppStyles.container}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>

                    <View style={{flex: 1}}>
                        {this.renderHeader()}
                        {this.renderDetails()}
                    </View>
                    {this.renderFooter()}
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
        deviceInfoContainer: {
            backgroundColor: AppColors.white,
            flexDirection: 'column',
            marginBottom: 10,
            padding: 10,
        },
        header: {
            backgroundColor: AppColors.white,
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 10,
        },
        iconContainer: {
            alignItems: 'center',
            backgroundColor: AppColors.overlay,
            borderColor: AppColors.gray,
            borderWidth: 1 / PixelRatio.get(),
            justifyContent: 'center',
        },
        section: {
            paddingBottom: 10,
            marginBottom: 10, flexDirection: 'column',
            borderBottomColor: AppColors.gray,
            borderBottomWidth: 1 / PixelRatio.get(),
        }
    }
);