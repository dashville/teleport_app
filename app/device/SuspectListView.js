import React from 'react';
import {Image, FlatList, NativeModules, PixelRatio, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import {AppColors, AppStyles, UITheme} from '../AppStyling'
const Intent = NativeModules.IntentModuleAndroid;
import MenuButton from '../components/navbar/MenuButton'
import Icon from 'react-native-vector-icons/Ionicons'
import {CachedImage}from 'react-native-cached-image'
import QuickSuspectView from './QuickSuspectView'
import AppUtils from '../utils/AppUtils'
export default class SuspectListView extends React.Component {
    constructor(props) {
        super(props)
        const {navigation} = props;
        const device = navigation.getParam('device');
        this.state = {
            suspects: _.reverse(device.suspects),
            selectedSuspect: null
        }
    }

    static navigationOptions = (props) => {
        return {
            title: 'Suspects',
            headerRight: <MenuButton {...props}/>,
            headerStyle: AppStyles.navbar
        }
    };

    _selectSuspect(suspect) {
        this.setState({selectedSuspect: suspect})
    }

    _hideQuickSuspectView() {
        this.setState({selectedSuspect: null})
    }

    renderItem(rowItem) {
        const {app} = this.props.screenProps;
        const suspect = rowItem.item
        const {width} = UITheme;
        const imageUrl = `${app.serverUrl}/images/${suspect.face}`
        const iconWidth = (width * 3 / 4) - 218
        const renderLine = (value) => {
            if (!value) {
                return null
            }
            const {textBold} = AppStyles
            return <Text style={_.extend({}, textBold, {flex: 1, flexWrap: 'wrap'})}>{`${value}`}</Text>
        }
        const formatDate = (tstamp) => {
            return moment(Number(AppUtils.toLocalTime(tstamp))).format('DD/MM/YYYY HH:mm')
        }
        return (
            <TouchableOpacity
                onPress={() => this._selectSuspect(suspect)}
                style={styles.renderItem}
            >
                <View
                    style={[styles.iconContainer, {
                        width: iconWidth,
                        height: iconWidth,
                        borderRadius: iconWidth / 2,
                        marginRight: 5
                    }]}>
                    <CachedImage source={{uri: imageUrl}}
                                 style={{width: iconWidth, height: iconWidth, borderRadius: iconWidth / 2}}/>
                </View>
                <View style={styles.detailsContainer}>
                    {renderLine(suspect.location.formattedAddress)}
                    {renderLine(formatDate(suspect.createdOn))}
                </View>
                <View style={{flex: 1}}>
                    <Icon name="ios-arrow-forward-outline" size={17}/>
                </View>
            </TouchableOpacity>
        )
    }

    renderFooter() {
        const {actionButton, actionIcon, textBold} = AppStyles;
        const navigate = () => {
            const {app} = this.props.screenProps
            const {navigation} = this.props
            app.replaceScreen(navigation, 'home')
        }
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {
                    margin: 10,
                })}
                onPress={navigate}>
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Home</Text>
                <Icon style={actionIcon} name="ios-home"/>
            </TouchableOpacity>
        )
    }

    renderHeader() {
        const count = _.size(this.state.suspects);
        const text = _.extend({}, AppStyles.textBold);
        return (
            <View style={{
                flexDirection: 'row', borderBottomColor: AppColors.gray,
                borderBottomWidth: 1 / PixelRatio.get(),
            }}>
                <Text style={[text, {flex: 1}]}>{`Showing: ${count}`}</Text>
            </View>
        )
    }

    render() {
        const {app} = this.props.screenProps;
        return (
            <View style={AppStyles.container}>
                {this.renderHeader()}
                <FlatList
                    style={styles.list}
                    initialNumToRender={7}
                    data={this.state.suspects}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={(item, index) => item.face + item.createdOn}
                />
                {this.renderFooter()}

                {this.state.selectedSuspect ?
                    <QuickSuspectView
                        app={app}
                        onClose={this._hideQuickSuspectView.bind(this)}
                        suspect={this.state.selectedSuspect}
                    /> : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    list: {padding: 10, flex: 1},
    renderItem: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: AppColors.gray,
        borderBottomWidth: 1 / PixelRatio.get(),
        backgroundColor: AppColors.white,
        padding: 2,
    },
    deviceInfoContainer: {
        backgroundColor: AppColors.white,
        flexDirection: 'column',
        marginBottom: 10,
        padding: 10,
        flex: 1
    },
})