import React from 'react'
import {
    View,
    Text,
    TouchableOpacity, NativeModules,
    PixelRatio,
    StyleSheet,
} from 'react-native'
import _ from 'lodash';
import moment from 'moment';
import {CachedImage}from 'react-native-cached-image';
import {AppStyles, AppColors, UITheme} from '../AppStyling';
import Icon from 'react-native-vector-icons/Ionicons'
const Intent = NativeModules.IntentModuleAndroid;
import AppUtils from '../utils/AppUtils'

const Footer = (props) => {
    const {actionButton, actionIcon, textBold} = AppStyles;
    return (
        <TouchableOpacity
            style={_.extend({}, actionButton, {margin: 10,})}
            onPress={props.navigate}>
            <Text style={_.extend({}, textBold, {color: AppColors.white})}>View Location</Text>
            <Icon style={actionIcon} name="ios-map"/>
        </TouchableOpacity>
    )
};
const QuickSuspectView = (props) => {
    const {app, onClose, suspect} = props
    const {coords, formattedAddress} = suspect.location
    const {width} = UITheme;
    const imgWidth = (width * 3 / 4) - 50
    const imageUrl = `${app.serverUrl}/images/${suspect.face}`
    const imgStyle = {
        width: imgWidth, height: imgWidth,
        borderWidth: 1 / PixelRatio.get(), borderColor: AppColors.gray
    }
    const formatDate = (tstamp) => {
        return moment(Number(AppUtils.toLocalTime(tstamp))).format('DD/MM/YYYY HH:mm');
    };
    const navigate = () => {
        Intent.navigateTo(coords.latitude, coords.longitude, formattedAddress)
    }
    return (
        <View
            style={[AppStyles.container, styles.container]}
        >
            <TouchableOpacity
                style={styles.closeBtn}
                onPress={onClose}
            >
                <Icon name="ios-close" style={{color: AppColors.white, fontSize: 40}}/>
            </TouchableOpacity>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                <View style={styles.imgContainer}>
                    <CachedImage
                        source={{uri: imageUrl}}
                        style={imgStyle}
                    />
                </View>
                <Text style={styles.text}>{formatDate(suspect.createdOn)}</Text>
                <Text style={styles.text}>{formattedAddress}</Text>
            </View>
            <Footer navigate={navigate}/>
        </View>
    )
};
const styles = StyleSheet.create({
    closeBtn: {
        position: 'absolute',
        top: 10,
        left: 10,padding:10
    },
    container: {
        backgroundColor: AppColors.overlayDark,
        position: 'absolute', bottom: 0, top: 0, left: 0, right: 0
    },
    imgContainer: {
        marginBottom: 10,
        borderRadius: 1,
        backgroundColor: AppColors.white,
        padding: 5, paddingBottom: 30,
    },
    text: _.extend({}, AppStyles.textBold, {marginBottom: 10, flexWrap: 'wrap', color: AppColors.white})
});
export default QuickSuspectView;