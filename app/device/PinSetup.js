import React from 'react';
import {ActivityIndicator,Alert, AsyncStorage, PixelRatio, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import _ from 'lodash'
import {AppColors, AppStyles, UITheme} from '../AppStyling'
import Icon from 'react-native-vector-icons/Ionicons'
import Camera from '../detection/Camera';
import AuthUtils from '../utils/AuthUtils';
import mime from 'react-native-mime-types'

const PIN_LENGTH = 4;
export  default class PinSetup extends React.Component {
    state = {
        initialPin: '',
        confirmationPin: '',
        initialPinReady: false,
        initialPinConfirmed: false,
        confirmationPinReady: false,
        isShowingCamera: true,
        isVerifying: false,
    }
    static navigationOptions = (props) => {
        return {
            header: null
        }
    };

    _verifyDevice(image) {
        const {app} = this.props.screenProps
        const url = `${app.serverUrl}/mobile/v1/device/${app.deviceInfo.uniqueId}/verify`
        const headers = app.requestHeaders();
        this.setState({isVerifying: true});
        fetch(url, {
            method: "GET",
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            }),
        }).then(app.responseToJson(url)).then(responseData => {
            this.setState({isVerifying: false})
            if(responseData.success){
                this.setState({isShowingCamera: false})
                const {item} = responseData;
                if(!(item && item.isOwner)){
                    Alert.alert("Info", "Would you like to add this device as one of yours?",
                        [
                            {text: 'No', onPress: () => {}, style: 'cancel'},
                            {text: 'Yes', onPress: () => this.addDevice()},
                        ], { cancelable: false })
                }
            } else {
                console.log('PinSetup._verifyDevice.imgage ===>', _.keys(image))
                this.addSuspect(image);
                Alert.alert("Info", "Device already registered, logging you out.")
            }
        }).catch(err => {
             this.setState({isVerifying: false})
            console.log('PineSetup.checkDevice.err ===>', err)
        })
    }
    addDevice() {
        const {app} = this.props.screenProps
        const url = `${app.serverUrl}/mobile/v1/device/add`;
        const headers = app.requestHeaders();
        const body = JSON.stringify({
            deviceInfo: app.deviceInfo,
            simData: app.simData,
            location: app.location,
        })
        app.showActicity()
        this.setState({isVerifying: true})
        fetch(url, {
            method: "POST",
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            }),
            body,
        }).then(app.responseToJson(url)).then(responseData => {
           app.hideActicity();
            if(responseData.success){
                Alert.alert("Info", "Device added.")
            } else {
                Alert.alert("Error", responseData.message)
            }
        }).catch(err => {
            app.hideActicity();
            console.log('PineSetup.addDevice.err ===>', err)
        })
    }
    addSuspect(data){
        const {app} = this.props.screenProps
        const url = `${app.serverUrl}/mobile/v1/device/add-suspect`
        const headers = app.requestHeaders()
        const form = new FormData()
        const uri = data.uri
        const name = uri.substring(uri.lastIndexOf('/') + 1)
        const type = mime.lookup(name)
        form.append('image', {
            uri,
            type,
            name,
        });
        fetch(url, {
            method: 'POST',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'multipart/form-data',
                'location': JSON.stringify(app.location),
            }), body: form
        }).then(app.responseToJson(url)).then((resp) => {
            console.log('PinSetup.addSuspect.responseData ===>', resp)
            AuthUtils.logout(app)
        }).catch((err) => {
            AuthUtils.logout(app)
            console.log('PinSetup.addSuspect.err ===>', err)
        })
    }

    pinPadHandler(key) {
        this.setState((prevState, props) => {
            if (prevState.initialPinReady && prevState.initialPinConfirmed && _.size(prevState.confirmationPin + key) <= PIN_LENGTH) {
                return {
                    confirmationPin: prevState.confirmationPin + key,
                    confirmationPinReady: _.size(prevState.confirmationPin + key) === PIN_LENGTH
                }
            } else if (!prevState.initialPinConfirmed && _.size(prevState.initialPin + key) <= PIN_LENGTH) {
                return {
                    initialPin: prevState.initialPin + key,
                    initialPinReady: _.size(prevState.initialPin + key) === PIN_LENGTH
                }
            }
            return {}
        });
    }

    pinPadClearHandler(key) {
        this.setState((prevState, props) => {
            if (prevState.initialPinConfirmed) {
                return {
                    confirmationPin: '',
                    confirmationPinReady: false,
                }
            }
            return {
                initialPin: '',
                initialPinReady: false
            }
        })
    }

    renderActivityIndicator() {
        if (!this.state.isVerifying) {
            return null
        }
        return (
            <View style={styles.activityIndicator}>
                <ActivityIndicator size={"large"} color={AppColors.white}/>
                <Text style={_.extend({}, AppStyles.textBold, {color: AppColors.white})}
                >Verifying...</Text>
            </View>
        )
    }

    renderPad() {
        const {width} = UITheme
        const btnWidth = (width * 3 / 4) - 215;
        const btnStyle = {
            width: btnWidth,
            height: btnWidth,
            borderRadius: btnWidth / 2,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 3,
            backgroundColor: AppColors.white,
            borderColor: AppColors.black,
            borderWidth: 1,
            marginHorizontal: 5,
            alignSelf: 'center'
        }
        const {initialPin, confirmationPin, initialPinConfirmed, initialPinReady} = this.state
        const pin = initialPinConfirmed ? confirmationPin : initialPin
        return (
            <View>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    height: 20, marginVertical: 10
                }}>
                    <Text style={styles.padText}>{ _.map(pin, c => {
                        return "*"
                    })}</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("1")}
                    >
                        <Text style={styles.padButton}>1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("2")}
                    >
                        <Text style={styles.padButton}>2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("3")}
                    >
                        <Text style={styles.padButton}>3</Text>
                    </TouchableOpacity>

                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("4")}
                    >
                        <Text style={styles.padButton}>4</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("5")}
                    >
                        <Text style={styles.padButton}>5</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("6")}
                    >
                        <Text style={styles.padButton}>6</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("7")}
                    >
                        <Text style={styles.padButton}>7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("8")}
                    >
                        <Text style={styles.padButton}>8</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("9")}
                    >
                        <Text style={styles.padButton}>9</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnContainer}>
                    {initialPinConfirmed ?
                        <TouchableOpacity
                            onPress={this._resetState.bind(this)}
                            style={btnStyle}
                        ><Icon name="ios-arrow-back-outline" style={styles.padButton}/>
                        </TouchableOpacity>
                        : <View style={{flex: 1}}/>
                    }

                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("0")}
                    >
                        <Text style={styles.padButton}>0</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={this.pinPadClearHandler.bind(this)}
                    >
                        <Icon name="ios-backspace-outline" style={styles.padButton}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    _savePin() {
        const {initialPin, confirmationPin} = this.state
        if (initialPin === confirmationPin) {
            const {app} = this.props.screenProps;
            const {navigation} = this.props
            app.showActicity()
            try {
                AsyncStorage.setItem(app.pinKey, confirmationPin, (err, pin) => {
                    app.hideActicity()
                    if (!err) {
                        app.replaceScreen(navigation, 'home')
                    }
                })
            } catch (e) {
                app.hideActicity()
                console.log('PinSetup._savePin.err ===>', e)
            }
        } else {
            Alert.alert("Error", "The pin isn't matching.");
            this._resetState()
        }
    }

    _resetState() {
        this.setState({
            initialPin: '',
            confirmationPin: '',
            initialPinReady: false,
            initialPinConfirmed: false,
            confirmationPinReady: false
        })
    }

    renderFooter() {
        const {actionButton, textBold} = AppStyles;
        const {initialPinReady, confirmationPinReady, initialPinConfirmed} = this.state

        if (initialPinConfirmed) {
            return (
                <TouchableOpacity
                    disabled={!confirmationPinReady}
                    style={_.extend({}, actionButton, {
                        margin: 10, backgroundColor: confirmationPinReady ? AppColors.primary : AppColors.gray
                    })}
                    onPress={this._savePin.bind(this)}>
                    <Text style={_.extend({}, textBold, {color: AppColors.white})}>Confirm</Text>
                </TouchableOpacity>
            )
        }
        return (
            <TouchableOpacity
                disabled={!initialPinReady}
                style={_.extend({}, actionButton, {
                    margin: 10, backgroundColor: initialPinReady ? AppColors.primary : AppColors.gray
                })}
                onPress={() => this.setState({initialPinConfirmed: true})}>
                <Text style={_.extend({}, textBold, {color: AppColors.white})}>Done</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const {initialPinConfirmed, isShowingCamera} = this.state

        if (isShowingCamera) {
            return (
                 <View style={AppStyles.container}>
                    <Camera
                        closeCamera={() => {
                        }}
                        hideCapture={true}
                        ref="cam"
                        uploadImage={this._verifyDevice.bind(this)}/>
                    {this.renderActivityIndicator()}
                </View>
            )
        }
        return (
            <View style={AppStyles.container}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.heading}
                    >{initialPinConfirmed ? 'Re-Enter Pin' : 'Enter Pin'}
                    </Text>
                    {this.renderPad()}
                </View>
                {this.renderFooter()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    activityIndicator: {
        alignItems: 'center',
        backgroundColor: AppColors.transparent,
        position: "absolute",
        top: 20,
        right: 0, left: 0,
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 5,
    },
    heading: _.extend({}, AppStyles.textBold, {fontSize: 18,}),
    text: _.extend({}, AppStyles.text),
    pinPad: {
        position: 'absolute', top: 0, right: 0, left: 0, bottom: 0,
    },
    padButton: _.extend({}, AppStyles.textBold, {color: AppColors.black, fontSize: 20}),
    padText: _.extend({}, AppStyles.textBold, {color: AppColors.black, fontSize: 40})
});