import React, {Component} from 'react'
import {View, ActivityIndicator, StyleSheet} from 'react-native'
import {AppColors} from './AppStyling'

export default class ActivityPanel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size={'large'} color={AppColors.primary}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute', top: 0, right: 0, left: 0, bottom: 0,
        backgroundColor: AppColors.transparent,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
