import React from 'react';
import {
    Alert, AsyncStorage,
    Platform, View, StyleSheet, InteractionManager, DeviceEventEmitter
} from 'react-native';
import _ from 'lodash'
import DeviceInfo from 'react-native-device-info';
import RNSimData from 'react-native-sim-data'
import {Routes} from './Routes'
import ActivityPanel from './ActivityPanel'
import ReconnectingSocket from './ReconnectingSocket'
import ConnectionPanel from './ConnectionPanel';
import Locking from './Modules'
import {NavigationActions} from 'react-navigation'
import AuthUtils from './utils/AuthUtils'

//const APP_URL = 'http://10.0.0.21:9000'
//const WS_URL = `ws://10.0.0.21:2222`
//const APP_URL = 'http://192.168.8.112:9000'
//const WS_URL = `ws://192.168.8.112:9000`
const APP_URL = 'https://teleserve.herokuapp.com'
const WS_URL = `ws://teleserve.herokuapp.com`
const Geolocation = navigator.geolocation

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showActivityPanel: false,
            navigation: null
        }
        this.serverUrl = APP_URL
        this.wsUrl = WS_URL
        this.tokenkey = 'x_token_x'
        this.hasHadUserKey = 'x_hasHadUserKey_x';
        this.screenLockKey = "x_lockKey_x";
        this.pinKey = "x_pin_x";
        this.locationKey = "x_key_x";
        this.systemId = 'teleport'
        this.appId = 'teleport_app'
        this.timeout = 3000;
        this.deviceInfo = {
            uniqueId: DeviceInfo.getUniqueID(), //Android: "dd96dec43fb81c97"
            deviceId: DeviceInfo.getDeviceId(), // Android: "goldfish",
            locale: DeviceInfo.getDeviceLocale(),// Android: "en-US"
            country: DeviceInfo.getDeviceCountry(), // "US",
            bundleId: DeviceInfo.getBundleId(), // "com.learnium.mobil
            buildNumber: DeviceInfo.getBuildNumber(), // Android: 4,
            brand: DeviceInfo.getBrand(), //samsung,
            manufacturer: DeviceInfo.getManufacturer(),//Android: "Google",
            timezone: DeviceInfo.getTimezone(), // "Africa/Tunis"
            userAgent: DeviceInfo.getUserAgent(), //Android:
            appVersion: DeviceInfo.getVersion()//Android: "1.0
            // firstInstallTime = DeviceInfo.getFirstInstallTime() // Android: 1517681764528
        }
        this.showActicity = this._onShowActivity.bind(this)
        this.hideActicity = this._onHideActivity.bind(this)
        this.requestHeaders = this._requestHeaders.bind(this)
        this.responseToJson = this._responseToJson.bind(this)
        this.onUserChanged = this._onUserChanged.bind(this)
        this.ping = this._ping.bind(this)
        this.getCurrentLocation = this._getCurrentLocation.bind(this)
        this.startWatch = this._startWatch.bind(this);
        this.setNav = this._setNav.bind(this);
        this.saveLocation = this._saveLocation.bind(this)
        this.replaceScreen = this._replaceScreen.bind(this)
        try {
            this.simData = {
                phoneNumber: RNSimData.getTelephoneNumber(),
                carrier: RNSimData.getCarrierName(),
                countryCode: RNSimData.getCountryCode(),
                simInfo: RNSimData.getSimInfo()
            }
        } catch (err) {
            console.log('App.constructor.simData.err ------>', err)
        }

        this.screenWakeUpListener = this._screenWakeUpListener.bind(this)
    }

    componentDidMount() {
        this.webSocket = new ReconnectingSocket({app: this})
        this.webSocket.register()
        this.getCurrentLocation()
        this.startWatch()

        this.screenWakeUpListener();
    }

    componentWillUnmount() {
        try {
            Geolocation.clearWatch(this.watchId)
        } catch (err) {
            console.log('App.componentWillUnmount.error====', err)
        }
    }

    _screenWakeUpListener() {
        DeviceEventEmitter.addListener("onScreenWakeUp", (m) => {
            switch (m) {
                case "show-screen-lock":
                    if (this.state.navigation) {
                        this.state.navigation.push('screen-lock')
                        _.defer(() => Locking.lockNow())
                    }
                    break;
                default:
                    break;
            }
            console.log('App._screenWakeUpListener. ***** ===>', m)
        })
    }

    _setNav(navigation) {
        this.setState({navigation})
    }

    _getCurrentLocation() {
        try {
            AsyncStorage.getItem(this.locationKey, (err, location) => {
                if (location) {
                    this.location = JSON.parse(location);
                }
                Geolocation.getCurrentPosition((loc) => {
                        this.webSocket._updatePosition(loc);
                    }, (err) => {
                        console.log('App.getCurrentLocation.error ******', err)
                    }//, {enableHighAccuracy: true}
                )
            })

        } catch (err) {
            console.log('App.getCurrentLocation.err ====>', err)
        }
    }

    _saveLocation(loc) {
        this.location = loc;
        AsyncStorage.setItem(this.locationKey, JSON.stringify(loc))
    }

    _startWatch() {
        const options = {}//{distanceFilter: 20, enableHighAccuracy: true}
        try {
            this.watchId = Geolocation.watchPosition((position) => {
                this.webSocket._updatePosition(position);
            }, (err) => {
                console.log('App.startWatch.error ******', err)
            }, options)
        } catch (err) {
            console.log('App.startWatch.err ====>', err)
        }
    }

    _onUserChanged(user) {
        this.setState({user})
        this.token = user.token;
    }

    _requestHeaders() {
        const user = this.state.user
        return _.extend({}, this.deviceInfo, {
            authorization: this.token,
            systemId: this.systemId,
            uniqueId: this.deviceInfo.uniqueId,
            deviceId: this.deviceInfo.deviceId,
            buildNumber: this.deviceInfo.buildNumber,
            appVersion: this.deviceInfo.appVersion,
            appId: this.appId
        }, user ? {
            userId: user.userId || user._id,
            username: user.username,
            provider: user.provider,
            token: this.token
        } : {})
    }

    _ping(route) {
        const app = this;
        const {navigation} = app.state
        const url = `${app.serverUrl}/mobile/v1/ping`
        const headers = app.requestHeaders()
        fetch(url, {
                method: 'GET', timeout: app.timeout,
                headers: _.extend({}, headers, {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                })
            }
        ).then(app.responseToJson(url)).then((resp) => {
            if (navigation){
                if(navigation.state && navigation.state.routeName === 'splash'){
                    AuthUtils.tokenLogin(app,navigation)
                }else{
                    navigation.push(route)
                }
            }
        }).catch((err) => {
            Alert.alert('Info', "Could't reach server");
            if (navigation) {
                navigation.push('login')
            }
            console.log('App.ping failed ----->', _.keys(err))
        }).done()
    }

    _responseToJson(url) {
        return (response) => {
            try {
                return response.json()
            } catch (err) {
                console.log('App.responseToJson. err ------>', err)
                return {success: false, message: `Error calling ${url}\n ${response} \n=> ${JSON.stringify(err)}`}

            }
        }
    }

    _replaceScreen = (navigation, routeName) => {
        navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName})
            ]
        }))
    };

    _onShowActivity() {
        this.setState({showActivityPanel: true})
    }

    _onHideActivity() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({showActivityPanel: false})
        })
    }

    render() {
        const screenProps = _.extend({}, {app: this});
        return (
            <View style={styles.container}>
                <Routes screenProps={screenProps}/>
                {this.state.showActivityPanel ? <ActivityPanel screenProps={screenProps}/> : null}

                <ConnectionPanel {...screenProps}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        flexDirection: 'column'
    }
})
