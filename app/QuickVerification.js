import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator,
    Alert, InteractionManager
} from 'react-native'
import _ from 'lodash'
import {AppStyles, AppColors} from './AppStyling'
import SmsListener from 'react-native-android-sms-listener'
import AuthUtils from './utils/AuthUtils'
import TimerCountdown from 'react-native-timer-countdown'

export default class QuickVerification extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: props.form,
            otp: '',
        }

    }

    componentDidMount() {
        this.MessageListner = SmsListener.addListener(message => {
            this._VerifyMessage(message)

        })
        this._requestOTP()
    }

    componentWillUnmount() {
        if (this.MessageListner) {
            this.MessageListner.remove()
        }
    }

    _VerifyMessage(msg) {
        // { body: '504608', originatingAddress: '+27820072381' }
        console.log(`QuickVerification._VerifyMessage. ${msg.body} ==${msg.originatingAddress}===> ${this.state.otp} `)
        if (msg.body == this.state.otp) {
            this._registerUser()
        } else {
            this._requestOTP()
        }
    }

    _registerUser() {
        const {app, navigation} = this.props
        const {form} = this.state
        const url = `${app.serverUrl}/mobile/v1/auth/register`;
        const headers = app.requestHeaders();
        app.showActicity()
        fetch(url, {
            method: 'POST',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-type': 'application/json'
            }), body: JSON.stringify(form)
        }).then(app.responseToJson(url)).then((respData) => {
            app.hideActicity()
            if (respData.success) {
                this.props.onSuccess()
                this._onClose()
            } else {
                //console.log('QuickVerification._register.===============>',respData.message)
                Alert.alert(
                    'error',
                    JSON.stringify(respData.message),
                    [{text: 'OK', onPress: () => this._onClose()}]
                )
            }
        }).catch((err) => {
            app.hideActicity()
            console.log('Authenitcate._onSignup err ---->', err)
        })
    }

    _requestOTP() {
        const {app} = this.props
        const {phone} = this.state.form //'+27724385753' //'+27670051754'
        const url = `${app.serverUrl}/mobile/v1/auth/opt/:${phone}`;
        const headers = app.requestHeaders();
        fetch(url, {
            method: 'GET',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-type': 'application/json'
            })
        }).then(app.responseToJson(url)).then((respData) => {
            if (respData.success) {
                this.setState({otp: respData.item})
            } else {
                //Show resend
            }
        }).catch((err) => {
            console.log('Authenitcate._requestOTP err ---->', err)
        })
    }

    _onClose() {
        this.props.onClose()
    }

    renderFooter() {
        return (
            <TouchableOpacity
                style={_.extend({}, AppStyles.actionButton, {
                    margin: 10,
                })}
                onPress={this._onClose.bind(this)}
            >
                <Text style={_.extend({}, AppStyles.textBold, {color: AppColors.white})}>Cancel</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const {phone} = this.state.form
        const {text, textBold} = AppStyles
        return (
            <View
                style={[AppStyles.container, styles.container]}
            >
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <View style={styles.innerContainer}>
                        <Text style={textBold}>
                            {`SMS sent to ${phone}`}
                        </Text>
                        <ActivityIndicator size="small" color={AppColors.primary}/>
                    </View>
                    <TimerCountdown
                        initialSecondsRemaining={60000}
                        interval={1000}
                        onTick={secondsRemaining => console.log('tick', secondsRemaining)}
                        onTimeElapsed={() => {
                                Alert.alert("Info", "Please confirm phone number")
                                this._onClose()
                        }}
                        allowFontScaling={true}
                        style={_.extend({},textBold,{color: AppColors.white,fontSize: 20, marginTop: 10})}
                    />
                </View>
                {this.renderFooter()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: AppColors.overlayDark,
        position: 'absolute', bottom: 0, top: 0, left: 0, right: 0
    },
    innerContainer: {
        borderRadius: 3,
        backgroundColor: AppColors.white,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center', flexDirection: 'row'
    }
})