import React from 'react';
import {ActivityIndicator, AsyncStorage, Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppColors, AppStyles, UITheme} from './AppStyling'
import _ from 'lodash';
import * as mime from 'react-native-mime-types'

import Camera from './detection/Camera'
import Icon from 'react-native-vector-icons/Ionicons'

class PinPad extends React.Component {
    state = {
        pin: '',
        showPad: false,
        isVerifying: false,
    }

    pinPadHandler(key) {
        this.setState((prevState, props) => {
            const pin = prevState.pin + key;
            const pinLength = _.size(pin)

            if (pinLength <= 4) {
                if (pinLength === 4) {
                    this.props.verifyPin(pin)
                }
                return {
                    pin
                }
            }
            return {}
        })
    }

    pinPadClearHandler(key) {
        this.setState((prevState, props) => {
            return {
                pin: ''
            }
        })
    }

    _togglePinPad() {
        this.setState(((prevState, props) => {
            return {showPad: !prevState.showPad}
        }))
    }

    renderPad() {
        const {width} = UITheme
        const btnWidth = (width * 3 / 4) - 215;
        const btnStyle = {
            width: btnWidth,
            height: btnWidth,
            borderRadius: btnWidth / 2,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: AppColors.transparent,
            borderColor: AppColors.white,
            borderWidth: 2,
            marginHorizontal: 5,
            alignSelf: 'center'
        }
        return (
            <View>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                }}>
                    <Text style={styles.padText}>{ _.map(this.state.pin, c => {
                        return "*"
                    })}</Text>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("1")}
                    >
                        <Text style={styles.padButton}>1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("2")}
                    >
                        <Text style={styles.padButton}>2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("3")}
                    >
                        <Text style={styles.padButton}>3</Text>
                    </TouchableOpacity>

                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("4")}
                    >
                        <Text style={styles.padButton}>4</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("5")}
                    >
                        <Text style={styles.padButton}>5</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("6")}
                    >
                        <Text style={styles.padButton}>6</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("7")}
                    >
                        <Text style={styles.padButton}>7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("8")}
                    >
                        <Text style={styles.padButton}>8</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("9")}
                    >
                        <Text style={styles.padButton}>9</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        onPress={this._togglePinPad.bind(this)}
                        style={btnStyle}
                    >
                        <Icon name="ios-arrow-down-outline" style={styles.padButton}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={() => this.pinPadHandler("0")}
                    >
                        <Text style={styles.padButton}>0</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={btnStyle}
                        onPress={this.pinPadClearHandler.bind(this)}
                    >
                        <Icon name="ios-backspace-outline" style={styles.padButton}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderFooter() {
        const {actionButton, textBold} = AppStyles;
        const btnStyle = {
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 5,
            alignSelf: 'flex-end',
        }

        return (
            <TouchableOpacity
                style={btnStyle}
                onPress={this._togglePinPad.bind(this)}>
                <Icon name="ios-keypad" color={AppColors.white}
                                      style={{fontSize: 35}}/>
            </TouchableOpacity>
        )
    }

    render() {
        const {showPad} = this.state
        const containerColor = {backgroundColor: showPad ? AppColors.overlay : AppColors.transparent}
        return (
            <View style={[styles.pinPad, containerColor]}
            >
                <View style={{flex: 1}}/>
                {showPad ? this.renderPad() : this.renderFooter()}
            </View>
        )
    }
}
export default class ScreenLock extends React.Component {
    state = {
        pin: null
    };

    componentDidMount() {
        this._mounted = true
        this._retrievePin();
    }

    componentWillUnmount() {
        this._mounted = false
    }

    static navigationOptions = (props) => {
        return {
            header: null
        }
    };

    _retrievePin() {
        const {app} = this.props.screenProps;
        try {
            AsyncStorage.getItem(app.pinKey, (err, pin) => {
                if (pin) {
                    this.setState({pin})
                }
            })
        } catch (e) {
            console.log('ScreenLock.fetchPin.err ===>', e)
        }
    }

    _verifyPin(pin) {
        const {app} = this.props.screenProps;
        const {navigation} = this.props
        if (this.state.pin === pin) {
            app.replaceScreen(navigation,"home");
        }
    }

    _verify(data) {
        if (!this.state.isVerifying) {
            console.log('ScreenLock._verify *****')
            const {app} = this.props.screenProps
            const {navigation} = this.props
            const url = `${app.serverUrl}/mobile/v1/auth/verify-face`
            const headers = app.requestHeaders()
            const form = new FormData()
            const uri = data.uri
            const name = uri.substring(uri.lastIndexOf('/') + 1)
            const type = mime.lookup(name)
            form.append('image', {
                uri,
                type,
                name,
            });
            this.setState({isVerifying: true})
            fetch(url, {
                method: 'POST',
                headers: _.extend({}, headers, {
                    'location': JSON.stringify(app.location),
                    'Accept': 'application/json', 'Content-Type': 'multipart/form-data'
                }), body: form
            }).then(app.responseToJson(url)).then((responseData) => {
                if (this._mounted) {
                    this.setState({isVerifying: false})
                    if (responseData.success) {
                        app.replaceScreen(navigation,"home");
                    } else {
                        Alert.alert("Attention", responseData.message)
                        this.refs.cam && this.refs.cam.resetState();
                    }
                }
            }).catch((err) => {
                this.setState({isVerifying: false})
                console.log('ScreenLock._verify.err ===>', err)
            })
        }
        return true
    }

    renderActivityIndicator() {
        if (!this.state.isVerifying) {
            return null
        }
        return (
            <View style={styles.activityIndicator}>
                <ActivityIndicator size={"large"} color={AppColors.white}/>
                <Text style={_.extend({}, AppStyles.textBold, {color: AppColors.white})}
                >Verifying...</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={AppStyles.container}>
                <Camera
                    ref="cam"
                    hideCapture={true} onClose={() => {
                }} uploadImage={this._verify.bind(this)}/>
                <PinPad ref="pin" verifyPin={this._verifyPin.bind(this)}/>
                {this.renderActivityIndicator()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    activityIndicator: {
        alignItems: 'center',
        backgroundColor: AppColors.transparent,
        position: "absolute",
        top: 20,
        right: 0, left: 0,
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 5,
    },
    pinPad: {
        //AppColors.transparent,
        position: 'absolute', top: 0, right: 0, left: 0, bottom: 0,
    },
    padButton: _.extend({}, AppStyles.textBold, {color: AppColors.white, fontSize: 20}),
    padText: _.extend({}, AppStyles.textBold, {color: AppColors.white, fontSize: 40})
});