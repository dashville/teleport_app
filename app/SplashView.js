import React, {Component} from 'react'
import AuthUtils from './utils/AuthUtils'
import {View, Text, TouchableOpacity} from 'react-native'
import {AppStyles, AppColors} from './AppStyling'
import _ from 'lodash'

export default class SplashView extends React.Component {
    state = {
        showFooter: false
    }
    static navigationOptions = (props) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        this._isMounted = true
         const {app} = this.props.screenProps
        this.autLogin();
        setTimeout(() => {
             !app.state.showActivityIndicator && this._isMounted && this.setState({showFooter: true})
        }, 5000)
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps === this.props) {
            this.autLogin();
        }
    }
    componentWillUnmount(){
        this._isMounted = false
    }

    autLogin() {
        const {app} = this.props.screenProps
        const {navigation} = this.props
        app.setNav(navigation)
        AuthUtils.tokenLogin(app, navigation)
    }

    renderFooter() {
        if(!this.state.showFooter) return null;
        const {textBold, actionButton} = AppStyles;
        return (
            <TouchableOpacity
                style={_.extend({}, actionButton, {margin: 10,
                    backgroundColor: AppColors.transparent})}
                onPress={this.autLogin.bind(this)}
            >
                <Text style={_.extend({}, textBold, {color: AppColors.primary})}>Reconnect</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const {textBold} = AppStyles;
        return (
            <View style={AppStyles.container}>
                <View style={{
                    flexDirection: 'column',
                    justifyContent: 'center', alignItems: 'center', flex: 1
                }}>
                    <Text style={[textBold, {fontSize: 40}]}>Teleport</Text>
                </View>
                {this.renderFooter()}
            </View>
        )
    }
}