import React, {Component} from 'react'
import * as mime from 'react-native-mime-types'
import _ from 'lodash'
import qs from "qs"
import SmsListener from 'react-native-android-sms-listener'

class AppUtils {
    constructor() {
        this.uploadImage = this._uploadImage.bind(this)
        this.messageListner = this._MessageListener.bind(this)
        this.toLocalTime = this.toLocalTime.bind(this)
    }

    toLocalTime(time) {
        const d = new Date(Number(time));
        const offset = (new Date().getTimezoneOffset() / 60) * -1;
        const n = new Date(d.getTime() + offset);
        return n.getTime();
    };

    _MessageListener() {
        SmsListener.addListener(message => {
            console.log('Message =============>', message)
        })
    }

    _uploadImage(app, data, commandId = '', onError, onSuccess, update = false) {
        const query = qs.stringify({commandId, update})
        const url = `${app.serverUrl}/mobile/v1/upload/image?${query}`
        const headers = app.requestHeaders()
        const form = new FormData()
        const uri = data.uri
        const name = uri.substring(uri.lastIndexOf('/') + 1)
        const type = mime.lookup(name)
        form.append('image', {
            uri,
            type,
            name,
        });
        app.showActicity();
        fetch(url, {
            method: 'POST',
            headers: _.extend({}, headers, {
                'Accept': 'application/json', 'Content-Type': 'multipart/form-data'
            }), body: form
        }).then(app.responseToJson(url)).then((resp) => {
            app.hideActicity();
            onSuccess && onSuccess(resp)
        }).catch((err) => {
            app.hideActicity();
            onError && onError(err)
        })
    }
}
module.exports = new AppUtils()