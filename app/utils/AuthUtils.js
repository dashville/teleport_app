import _ from 'lodash'
import {AsyncStorage} from 'react-native'
import RNRestart from 'react-native-restart'

class AuthUtils {
    constructor() {
        this.tokenLogin = this._tokenLogin.bind(this)
        this.loginSuccessful = this._loginSuccessful.bind(this)
        this.logout = this._logout.bind(this)
        this.fetchToken = this._fetchToken.bind(this);
        this.setHasHadUser = this._setHasHadUser.bind(this)
    }

    _loginSuccessful(app, navigation, user, token) {
        const onUserChange = () => {
            try {
                AsyncStorage.getItem(app.pinKey, (err, pin) => {
                    const userObj = _.extend({}, user, {token})
                    app.onUserChanged(userObj)
                    _.defer(() => {
                        if (user.hasIdentity) {
                            const screenPath = pin ? 'home' : 'auth-pin';
                            app.replaceScreen(navigation, screenPath)
                        } else {
                            app.replaceScreen(navigation, 'welcome')
                        }
                    })
                })
            } catch (e) {
                console.log('AuthUtils.fetchPin.err ===>', e)
            }
        };
        try {
            this._setHasHadUser(app)
            this._fetchToken(app, token, onUserChange)
        } catch (err) {
            console.log('AuthUtils._loginSuccessful error', err)
        }
    }

    _setHasHadUser(app) {
        AsyncStorage.getItem(app.hasHadUserKey, (err, hasHadUser) => {
            if (hasHadUser === null || hasHadUser === undefined) {
                try {
                    AsyncStorage.setItem(app.hasHadUserKey, 'true')
                } catch (e) {

                }
            }
        })
    }

    _fetchToken(app, token, onUserChange) {
        AsyncStorage.getItem(app.tokenkey, (err, existingToken) => {
            if (existingToken === token) {
                onUserChange()
            } else {

                AsyncStorage.setItem(app.tokenkey, token, (err, savedToken) => {
                    console.log('AuthUtils._loginSuccessful.err======>', err)
                    console.log('AuthUtils._loginSuccessful.token======>', token)
                    console.log('AuthUtils._loginSuccessful.app.tokenkey======>', app.tokenkey)
                    onUserChange()
                });
            }
        })
    }

    _logout(app) {
        try {
            AsyncStorage.removeItem(app.tokenkey);
            _.defer(() => {
                RNRestart.Restart();
            })
        } catch (err) {
            console.log('AuthUtils._logout err', err)
        }
    }

    _validateToken(app, navigation, token) {
        console.log('AuthUtils._validateToken. ===>')
        const url = `${app.serverUrl}/mobile/v1/auth/validate-token`;
        const headers = app.requestHeaders();
        app.showActicity()
        fetch(url, {
            method: 'POST', timeout: app.timeout,
            headers: _.extend({}, headers, {
                authorization: token,
                'Accept': 'application/json', 'Content-Type': 'application/json'
            })
        }).then(app.responseToJson(url)).then((respData) => {
            app.hideActicity()
            if (respData.success) {
                this.loginSuccessful(app, navigation, respData.item, respData.token)
            } else {
               app.replaceScreen(navigation, 'auth')
            }
        }).catch((err) => {
            app.hideActicity();
            app.ping();
            console.log('AuthUtils._tokenLogin fetch err ---->', err)
        })
    }

    _getToken(tokenkey) {
        return new Promise((resolve, reject) => {
            try {
                AsyncStorage.getItem(tokenkey, (error, token) => {
                    if (error) reject(error)
                    resolve(token)
                })
            } catch (err) {
                reject(err)
            }
        })
    }

    _tokenLogin(app, navigation) {
        console.log('AuthUtils._tokenLogin.token**********')
        this._getToken(app.tokenkey).then(token => {
            console.log('AuthUtils._tokenLogin.token ===>', token)
            if (token) {
                this._validateToken(app, navigation, token)
            } else {
                app.replaceScreen(navigation, 'auth')
            }
        }).catch(err => {
            console.log('AuthUtils._tokenLogin. err ---->', err)
        })
    }
}
module.exports = new AuthUtils()