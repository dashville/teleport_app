package com.teleport_app;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.widget.Toast;
import android.view.View;
import android.content.BroadcastReceiver;
import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.IBinder;


public class LockingModule extends ReactContextBaseJavaModule {
    private DevicePolicyManager devicePolicyManager;
    private ComponentName myAdmin;
    private final int REQUEST_CODE_ENABLE_ADMIN = 11;
    private Context context;
    private ReactContext mReactContext;

    public LockingModule(ReactApplicationContext reactContext){
        super(reactContext);
        Activity currentActivity = getCurrentActivity();
        this.mReactContext = reactContext;
        context = reactContext.getApplicationContext();
        devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        myAdmin = new ComponentName(reactContext, MyAdmin.class);
    }

    @Override
    public String getName(){
        return "Locking";
    }

    protected void wakeUp(){

    }

    @ReactMethod
    public void lock(){
      boolean active = isActiveAdmin();
        if(active){
        	mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onScreenWakeUp", "show-screen-lock");
        } else {
          //  Toast.makeTest(LockingModule.this, "You to enable the admin Device Feature", Toast.LENGTH_SHORT).show();
        }
    }
     @ReactMethod
     public void lockNow(){
        boolean active = isActiveAdmin();
        if(active){
        	devicePolicyManager.lockNow();
        }
     }
    @ReactMethod
    public void enable(){
        Activity currentActivity = getCurrentActivity();
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, myAdmin);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "To enable device lock, change permission");
        currentActivity.startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
    }
    @ReactMethod
    public void startService(){

        Intent i = new Intent();
        i.setClass(this.getReactApplicationContext(), LockScreenService.class);
        getReactApplicationContext().startService(i);
        mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onScreenWakeUp", "startService");
    }

    @ReactMethod
    public void disable(){
        devicePolicyManager.removeActiveAdmin(myAdmin);
    }
    @ReactMethod
    public boolean isActiveAdmin(){
        return devicePolicyManager.isAdminActive(myAdmin);
    }
    //@Override
    public void onBackPressed() {
        return; //Do nothing!
    }

    @ReactMethod
    public void unlockScreen(View view) {
        //Instead of using finish(), this totally destroys the process
        android.os.Process.killProcess(android.os.Process.myPid());
    }


    class LockScreenReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("onScreenWakeUp", "Test");

            //If the screen was just turned on or it just booted up, start your Lock Activity
            if(action.equals(Intent.ACTION_SCREEN_OFF) || action.equals(Intent.ACTION_BOOT_COMPLETED)){
                String eventName = "onScreenWakeUp";
                mReactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, true);
            }
        }
    }
    class LockScreenService extends Service {

        BroadcastReceiver receiver;

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        @SuppressWarnings("deprecation")
        public void onCreate() {
            KeyguardManager.KeyguardLock key;
            KeyguardManager km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);

            //This is deprecated, but it is a simple way to disable the lockscreen in code
            key = km.newKeyguardLock("IN");

            key.disableKeyguard();
            //Start listening for the Screen On, Screen Off, and Boot completed actions
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_BOOT_COMPLETED);
            //Set up a receiver to listen for the Intents in this Service
            receiver = new LockScreenReceiver();
            registerReceiver(receiver, filter);
            super.onCreate();
        }

        @Override
        public void onDestroy() {
            unregisterReceiver(receiver);
            super.onDestroy();
        }
    }

}