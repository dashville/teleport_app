package com.teleport_app;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class IntentModule extends ReactContextBaseJavaModule {
    public IntentModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void navigateTo(Float toLat, Float toLon, String name) {
        //Uri uri = Uri.parse("geo:" + toLat + "," + toLon);
        Uri uri = Uri.parse("geo:0,0?q=" + toLat + "," + toLon + " (" + name.replace(')',']').replace('(',']') + ")" );
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        getCurrentActivity().startActivity(mapIntent);
    }

    @Override
    public String getName() {
        return "IntentModuleAndroid";
    }
}
