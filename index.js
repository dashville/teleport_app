import { AppRegistry } from 'react-native';
import App from './app/App';

AppRegistry.registerComponent('teleport_app', () => App);
