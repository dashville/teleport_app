#!/bin/bash
project_home=$(cd `dirname $0`/..; pwd)

cd $project_home
assets_dir="android/app/src/main/assets/fonts"
if [ ! -d $assets_dir ]; then
  mkdir -p  $assets_dir
  cp -rf node_modules/react-native-vector-icons/Fonts/Ionicons.ttf $assets_dir
elif [ ! -f $assets_dir/Ionicons.ttf ]; then
  cp -rf node_modules/react-native-vector-icons/Fonts/Ionicons.ttf $assets_dir
fi

cd $project_home/android && ./gradlew assembleRelease

cd $project_home && react-native bundle --platform android --dev false --entry-file index.js \
     --bundle-output android/app/src/main/assets/index.bundle
       


